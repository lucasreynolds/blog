import os
import webapp2
import jinja2
import logging
import hashlib
import hmac
import random
from string import letters
import secret
s = secret.secret()

from google.appengine.api import memcache
from google.appengine.ext import db
from google.appengine.api import users

template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir), autoescape=True)

def render_str(template, **params):
    t = jinja_env.get_template(template)
    return t.render(params)
    
### COOKIE FUNCTIONS ###
def make_secure_val(val, secret):
	return '%s|%s' % (val, hmac.new(secret, val).hexdigest())
	
def check_secure_val(secure_val, secret):
	val = secure_val.split('|')[0]
	if secure_val == make_secure_val(val):
		return val

### MAIN HANDLER ### 		
class Handler(webapp2.RequestHandler):
    def write(self, *a, **kw):
       	self.response.out.write(*a, **kw)
       	
    def render_str(self, template, **params):
        return render_str(template, **params)
        
    def render(self, template, **kw):
        self.write(self.render_str(template, **kw))
		
	# reads the cookie which has a name and a val
    def read_secure_cookie(self, name):
        cookie_val = self.request.cookies.get(name)
        return cookie_val and check_secure_val(cookie_val)
        
    def logout(self):
        self.response.headers.add_header('Set-Cookie', 'user_id=; Path=/')
    
    def initialize(self, *a, **kw):
        webapp2.RequestHandler.initialize(self, *a, **kw)
        uid = self.read_secure_cookie('user_id') 

### POSTS ###
# create a class of the blog which has title, content, and two dates.
class Post(db.Model):
	subject = db.StringProperty(required=True)
	content = db.TextProperty(required=True)
	name = db.StringListProperty(required=True)
	comment = db.StringListProperty(required=True)
	created = db.DateTimeProperty(auto_now_add=True)
	last_modified = db.DateTimeProperty(auto_now = True)
	
	
	# change \n with <br> so the blog out is spaced correctly
	def render(self):
		self._render_text = self.content.replace('\n', '<br>')
		return render_str

### ADMIN USER ###
def make_salt(length = 5):
    return ''.join(random.choice(letters) for x in xrange(length))

def make_pw_hash(name, pw, salt = None):
    if not salt:
        salt = make_salt()
    h = hashlib.sha256(name + pw + salt).hexdigest()
    return '%s,%s' % (salt, h)

def valid_pw(name, password, h):
    salt = h.split(',')[0]
    return h == make_pw_hash(name, password, salt)
    
class User(db.Model):
	user = db.StringProperty(required=True)
	password = db.StringProperty(required=True) #salt , password_hash
	secret = db.StringProperty(required=True)
	
	def login(self, u, p):
		return valid_pw(u, p, self.password)
            
class Login(Handler):
	def get(self):
		self.render("login.html", error="")
	def post(self):
		name = self.request.get("user_id")
		password = self.request.get("password")
		u = User.all().get()
		if u.login(name, password):
			self.set_secure_cookie(u)
		self.redirect('/blog')
	
	def set_secure_cookie(self, user):
		logging.error(user.secret.encode('ascii', 'ignore'))
		cookie_val = make_secure_val(str(user.key().id()), user.secret.encode('ascii', 'ignore'))
		self.response.headers.add_header('Set-Cookie', 'user_id=%s|%s; Path=/' % ((user.user).encode('ascii', 'ignore'), cookie_val))
			
class Logout(Handler):
	def get(self):
		self.logout()
		self.redirect('/blog')

class Register(Handler):
	def get(self):
		self.render("login.html", error="")
	def post(self):
		name = self.request.get("user_id")
		password = self.request.get("password")
		user_query = User.all().filter("user=", name).get()
		if not user_query and name == "lucasreynolds":
			u = User(user=name, password=make_pw_hash(name, password), secret=make_salt(random.randint(3, 7)))
			u.put()
		self.redirect('/blog')

# add posts to memcache	
def add_post(post):
	post.put()
	memcache.set("POST_"+ str(post.key().id()), post)
	return str(post.key().id())
	
def get_posts():
	##set each post to a small int starting from 1 to .id().key()
	q = Post.all().order('-created')
	mc_key = 'POST_'

	posts = memcache.get_multi("([0-9]+)", key_prefix = mc_key)
	if not posts:
		posts = list(q)
		for post in posts:
			memcache.set("POST_" + str(post.key().id()), post)
	return posts

### HANDLERS ###
class Admin(Handler):
	def render_newpost(self, subject="", content="", secret="", error=""):
		self.render("newpost.html", subject=subject, content=content, error=error)
		
	def get(self):
		self.render_newpost()
		
	def post(self):
		subject = self.request.get("subject")
		content = self.request.get("content")
		secret = self.request.get("secret")
		ip = self.request.remote_addr
		if subject and content and secret == s:
			#35.13.30.180
			post = Post(subject = subject, content = content, name=[], comment=[])
			self.redirect("/blog/%s" % add_post(post))
		else:
			self.render_newpost(subject, content, secret, "Error!!!")
			
class MainPage(Handler):
    def render_main_page(self):
    	Posts = get_posts()
    	self.render("mainpage.html", Posts=Posts)
    def get(self):
    	self.render_main_page()
        
class MainPageRedirect(Handler):
	def get(self):
		self.redirect('/blog')

class PostPage(Handler):
	def render_post(self, post_id, name="", error=""):
		post = memcache.get('POST_'+post_id)
		name_comment = []
		if not post:
			self.error(404)
			return
		if post.comment != [] and post.name != []:
			for n, c in zip(post.name, post.comment):
				name_comment.append((n, c))
		self.render("post.html", post=post, name=name, error=error, name_comment=name_comment)
		
	def get(self, post_id):
		self.render_post(post_id=post_id)
	
	def post(self, post_id):
		name = self.request.get("name")
		comment = self.request.get("comment")

		if comment:
			post = memcache.get('POST_'+post_id)
			p = post
			if not name:
				name = "Anonymous"
			p.name.append(name)
			p.comment.append(comment)
			p.put()
			memcache.set('POST_'+post_id, p)
			logging.error("COMMENT")
			self.render_post(post_id=post_id)
		else:
			self.render_post(post_id=post_id, name=name, error="Please include content in the comment section.")

class Flush(Handler):
	def render_page(self, secret="", error=""):
		self.render("flush.html", secret=secret, error=error)
	def get(self):
		self.render_page()
		
	def post(self):
		secret = self.request.get("secret")
		if secret == s:
			memcache.flush_all()
			posts = Post.all()
			for p in posts:
				p.delete()
			self.redirect("/blog")
		else:
			self.render_page(secret=secret, error="Error!!!")
	
app = webapp2.WSGIApplication([('/blog', MainPage),
 							   ('/blog/([0-9]+)', PostPage),
 							   ('/', MainPageRedirect),
 							   ('/blog/', MainPageRedirect),
 							   ('/submit', Admin),
 							   ('/login', Login),
 							   ('/logout', Logout),
 							   ('/register', Register),
 							   ('/flush', Flush)
 							   ], debug=True)
